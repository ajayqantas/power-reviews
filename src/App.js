import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
// import {POWERREVIEWS} from ''
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <h1 className="App-title">This is for a jacket</h1>
        </header>
        
        <div>
          <a href=""> Add a review </a>
        </div>
        <div style={{padding: '40px'}}>
          <div id="pr-reviewsnippet" />
          <div id="pr-write" />
          <div id="pr-reviewdisplay" />
        </div>
        
      </div>
    );
  }
}

export default App;
